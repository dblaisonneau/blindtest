# Come as you are - Nirvana

Viens comme tu es, comme tu étais
Comme je veux que tu sois
Comme un ami, comme un ami, comme un vieil ennemi
Prends ton temps, dépêche-toi
Le choix est tien, ne sois pas en retard
Prends du repos, comme un ami, comme un vieux souvenir
Souvenir (x3)


Come as you are, as you were
Viens comme tu es, comme tu étais
As I want you to be
Comme je veux que tu sois
As a friend, as a friend, as an old enemy
Comme un ami, comme un ami, comme un vieil ennemi
Take your time, hurry up
Prends ton temps, dépêche-toi
The choice is yours, don't be late
Le choix est tien, ne sois pas en retard
Take a rest, as a friend, as an old memoria
Prends du repos, comme un ami, comme un vieux souvenir
Memoria (x3)
Souvenir (x3)

# Bohemian Rhapsody - Queen

Is this the real life, is this just fantasy
Est-ce cela la vraie vie, ou est-ce seulement l'imagination ?
Caught in a landslide, no escape from reality
Pris dans un glissement de terrain, aucun échappatoire à la réalité
Open your eyes, look up to the skies and see
Ouvre les yeux, regarde les cieux et vois
I'm just a poor boy, I need no sympathy
Je ne suis qu'un pauvre garçon, je n'ai besoin d'aucune compassion
Because I'm easy come, easy go, little high, little low
Car ça va et ça vient, il y a des hauts et des bas
Anyway the wind blows, doesn't really matter to me
Peu importe par où le vent souffle, ce n'est pas vraiment important,
To me
Pour moi


# Highway To Hell - AC/DC

Livin' easy, livin' free
Vivre facilement, vivre libre
Season ticket on a one way ride
Un abonnement saisonnier pour un voyage sans retour
Asking nothing, leave me be
Ne demandant rien, laisse moi vivre
Taking everything in my stride
Tout prendre sur mon passage
Don't need reason, don't need rime
Je n'ai pas besoin de raisons, pas besoin de rime
Ain't nothing I would rather do
Il n'y a rien d'autre que j'aurais préféré faire
Going down, for a time
M’écrouler pour un moment
My friends are gonna be there too
Mes amis seront aussi là-bas

I'm on the highway to hell
Je suis sur l'autoroute de l'enfer
On the highway to hell
Sur l'autoroute de l'enfer
Highway to hell
L'autoroute de l'enfer
I'm on the highway to hell
Je suis sur l'autoroute de l'enfer

# Du Hast (You Have) - Rammstein
(Tu As)

(Chorus 1) (x4)
Du (You)
Toi
Du hast (You have)
Tu as
Du hast mich (You have)
Tu m'as

(Chorus 2)
Du (You)
Toi
Du hast (You have)
Tu as
Du hast mich (You have)
Tu m'as
Du hast mich (You have)
Tu m'as
Du hast mich gefragt (You have asked me)
Tu m'as demandé
Du hast mich gefragt (You have asked me)
Tu m'as demandé
Du hast mich gefragt (You have asked me)
Tu m'as demandé
Und ich hab nichts gesagt (And I did not obey)
Et je n'ai rien dit
